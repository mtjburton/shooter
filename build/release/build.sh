#!/bin/bash
MAGENTA='\033[0;35m'
CYAN='\033[0;36m'
NC='\033[0m' # No Color

mkdir -p shaders

echo -e ${MAGENTA}Copying shader files...${NC}

cp ../../src/shaders/* ./shaders

echo -e ${MAGENTA}Building lib...${NC}

clang++ -std=c++0x -Wall -O3 -Werror -Wno-c++11-extensions -D GLM_FORCE_RADIANS -D LOGGING -framework SDL2 -framework OpenGL -lGLEW -shared -o libshooter.so ../../src/build.cpp 

echo -e ${MAGENTA}Building exe...${NC}
clang++ -std=c++0x -Wall -O3 -Werror -Wno-c++11-extensions -D GLM_FORCE_RADIANS -D LOGGING -framework SDL2 -framework OpenGL -lGLEW -o shooter ../../src/main.cpp 

echo -e ${CYAN}Done${NC}
