#pragma once

class StackAllocator;
class RenderingSystem;
struct Player;
class ParticleSystem;

struct Globals
{
public:
    StackAllocator* pBSA;
    RenderingSystem* pRenderingSystem;
    Player* pPlayer;
    ParticleSystem* pPlayerEngine;
};

Globals* g;
