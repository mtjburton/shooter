#pragma once

#include "RenderingSystem.h"
#include "Transform.h"
#include "RigidBody.h"

struct Player
{
    Transform transform;
    Mesh mesh;
    RigidBody rigidBody;
};

struct Enemy
{
    Transform transform;
    Mesh mesh;
    RigidBody rigidBody;
};
