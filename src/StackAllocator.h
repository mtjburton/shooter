#pragma once

#include <iostream>
#include <assert.h>

class StackAllocator
{
public:
    StackAllocator(int* pMemory, size_t size)
    {
        m_pMemory = pMemory;
        m_pStackTop = m_pMemory;
        m_size = static_cast<long>(size);
    }

    int* Alloc(size_t size)
    {
        int* allocated = m_pStackTop;
        m_pStackTop += size;

#ifdef CHECKS
        if(m_pStackTop - m_pMemory > m_size)
        {
            std::cerr << "\033[0;31mSimple allocator has run out of memory!\033[0m"
                << std::endl;
            assert(0);
        }
#endif

        return allocated;
    }

    template<typename T>
    int* Alloc()
    {
        return Alloc(sizeof(T));
    }

    template<typename T>
    T* New()
    {
        return new(Alloc<T>()) T();
    }

private:
    int* m_pMemory;
    int* m_pStackTop;
    long m_size;
};

