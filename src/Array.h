#pragma once

#include <iostream>
#include "Globals.h"
#include "StackAllocator.h"

//Hardcoded to use global stack allocator, should really DI this

template <typename T>
class Array
{
    Array(int size)
    {
        m_objectSize = sizeof(T);
        m_pItems = new(g->pBSA->Alloc(m_objectSize * size)) T[size];
    }

    void Push(T* pItem)
    {
        m_pItems[m_next++] = pItem;

#ifdef CHECKS
        if(m_next >= m_objectSize)
        {
            std::cerr << "\033[0;31mArray is overflowing!\033[0m" << std::endl;
            assert(0);
        }
#endif
    }

    void Remove(int i)
    {
        m_next--;
#ifdef CHECKS
        if(i < m_next)
        {
            std::cerr << "\033[0;31mArray not big enough to remove that item!\033[0m"
                << std::endl;
            assert(0);
        }
#endif
        m_pItems[i] = m_pItems[m_next-1];
    }

    T operator[](int i)
    {
        return m_pItems[i];
    }

    T Get(int i)
    {
        return this[i];
    }

    int Size()
    {
        return m_next;
    }

    private:
    T* m_pItems;
    int m_next;
    size_t m_objectSize;
};
