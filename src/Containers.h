#pragma once

#include <iostream>

#include "Globals.h"
#include <assert.h>
#include "StackAllocator.h"

//Hardcoded to use global stack allocator, should really DI this

template <typename T>
class Stack
{
public:
    Stack(int size)
    {
        m_objectSize = sizeof(T);
        m_pItems = new(g->pBSA->Alloc(m_objectSize * size)) T[size];
        m_next = 0;
    }

    void Push(T* pItem)
    {
        m_pItems[m_next++] = pItem;

#ifdef CHECKS
        if(m_next >= m_objectSize)
        {
            std::cerr << "\033[0;31mStack is overflowing!\033[0m" << std::endl;
            assert(0);
        }
#endif
    }

    T Pop()
    {
        T pItem = m_pItems[--m_next];

#ifdef CHECKS
        if(m_next < 0)
        {
            std::cerr << "\033[0;31mStack is underflowing!\033[0m" << std::endl;
            assert(0);
        }
#endif

        return pItem;
    }

    T Top()
    {
        return m_pItems[m_next -1];
    }

    int Size()
    {
        return m_next;
    }

private:
    T* m_pItems;
    int m_next;
    size_t m_objectSize;
};
