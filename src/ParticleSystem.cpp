#include "ParticleSystem.h"
#include "PoolAllocator.h"
#include "Particle.h"

//TODO: Accessing the player directly is a bit too hacky for my liking
#include "Entity.h"

ParticleSystem::ParticleSystem()
{
    m_spawnedCount = 0;
    m_objectSize = sizeof(Particle);
    m_poolSize = 0;
    //Tight coupling to the allocator but meh
    m_pItems = new(g->pBSA->Alloc(m_objectSize * m_poolSize)) Particle[m_poolSize];
    m_particleSpawnWait = 0.1f;
}

void ParticleSystem::Init(int size, Mesh m)
{
    m_poolSize = size;
    m_particleMesh = m;
}

Particle* ParticleSystem::Get(int index)
{
#ifdef CHECKS
    if(index >= m_poolSize)
    {
        std::cerr << "\033[0;31mPool index out of bounds!\033[0m"
            << std::endl;
        assert(0);
    }
#endif
    return &m_pItems[index];
}

void ParticleSystem::Spawn(float lifetime, glm::vec3 pos)
{
#ifdef CHECKS
    if(m_spawnedCount >= m_poolSize)
    {
        std::cerr << "\033[0;31mPool has run out of objects!\033[0m"
            << std::endl;
        assert(0);
    }
#endif

    Particle* pItem = nullptr;
    //Could be unfied with the above, but I like to keep my preprocessor
    //conditions separate
    if(m_spawnedCount < m_poolSize)
    {
        pItem = &m_pItems[m_spawnedCount++];
        pItem->transform.MoveTo(pos);
        pItem->lifetime = lifetime;
        pItem->mesh = m_particleMesh;
        pItem->transform.Scale(0.05f);
    }
}

void ParticleSystem::Update(float dt)
{
    //TODO: Need to spawn particles faster than once per frame
    m_particleSpawnWait -= dt;
    if(m_particleSpawnWait < 0)
    {
        Spawn(0.3, g->pPlayer->transform.GetPosition());
        m_particleSpawnWait = 0.005;
    }

    for(int i = 0; i < m_spawnedCount; i++)
    {
        Particle* p = &m_pItems[i];

        if(p->lifetime <= 0)
        {
            m_spawnedCount--;
            m_pItems[i] = m_pItems[m_spawnedCount];
            m_pItems[m_spawnedCount] = Particle();
            continue;
        }

        p->transform.Rotate(glm::rotate(-0.1f, glm::vec3(0.0f, 0.0f, 1.0f)));
        g->pRenderingSystem->Draw(&p->mesh, p->transform.Mat4());
        p->lifetime -= dt;
    }
}
