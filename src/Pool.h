#pragma once

#include <iostream>

#include "Globals.h"
#include "StackAllocator.h"

template<typename T>
class Pool
{
public:
    Pool();
    ~Pool();

    void Init(int size);

    T* Spawn();
    void Kill(T* pItem);

    T* Get(int i);
    int AliveCount();

private:
    T* m_pItems;
    int m_poolSize;
    size_t m_objectSize;
    int m_aliveCount;
};

template<typename T>
Pool<T>::Pool()
{
    m_poolSize = 0;
    m_objectSize = 0;
    m_pItems = nullptr;
    m_aliveCount = 0;
}

template<typename T>
void Pool<T>::Init(int size)
{
    m_poolSize = size;
    m_objectSize = sizeof(T);

    //Tight coupling to the allocator but meh
    m_pItems = new(g->pBSA->Alloc(m_objectSize * m_poolSize)) T[m_poolSize];
}

template<typename T>
T* Pool<T>::Spawn()
{
#ifdef CHECKS
    if(m_aliveCount >= m_poolSize)
    {
        std::cerr << "\033[0;31mPool has run out of objects!\033[0m"
            << std::endl;
        assert(0);
    }
#endif

    return &m_pItems[m_aliveCount++];
}

template<typename T>
void Pool<T>::Kill(T* pItem)
{
    *pItem = m_pItems[--m_aliveCount];
}


template<typename T>
T* Pool<T>::Get(int i)
{
    return m_pItems[i];
}

template<typename T>
int Pool<T>::AliveCount()
{
    return m_aliveCount;
}
