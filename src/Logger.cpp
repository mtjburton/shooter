#include "logger.h"

#include <iostream>

void Log(glm::vec2 v, std::string s)
{
    std::cout << "[" << s << " (" << v.x << "," << v.y << ")]" << std::endl;
}

void Log(glm::vec3 v, std::string s)
{
    std::cout << "[" << s << " (" << v.x << "," << v.y << "," << v.z << ")]" << std::endl;
}

void Log(glm::vec4 v, std::string s)
{
    std::cout << "[" << s << " (" << v.x << "," << v.y <<
        "," << v.z << "," << v.w << ")]" << std::endl;
}

void Log(glm::mat4 m, std::string s)
{
    Log(m[0], s + "0");
    Log(m[1], s + "1");
    Log(m[2], s + "2");
    Log(m[3], s + "3");
}

void Log(int x, std::string s)
{
    std::cout << "[" << s << " (" << x << ")]" << std::endl;
}

void Log(float x, std::string s)
{
    std::cout << "[" << s << " (" << x << ")]" << std::endl;
}

void Log(double x, std::string s)
{
    std::cout << "[" << s << " (" << x << ")]" << std::endl;
}

void Log(std::string x, std::string s)
{
    std::cout << "[" << s << " (" << x << ")]" << std::endl;
}
