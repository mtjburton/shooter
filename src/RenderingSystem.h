#pragma once

#include <GL/glew.h>
#include <iostream>
#include <vector>

#include "glm/glm.hpp"

#include "Mesh.h"

struct SDL_Window;

class RenderingSystem
{
public:
    RenderingSystem();
    ~RenderingSystem();

    void Init();
    void ClearScreen();
    void Draw(Mesh* pMesh, glm::mat4 mvMatrix);
    void DisplayBackBuffer();
    Mesh BuildMesh(MeshData md);

private:
	std::string LoadShaderFile(std::string filename);
	GLuint MakeShader(GLenum type, std::string src);
	GLuint MakeBuffer(GLenum type, const void* data, GLsizei size);
	GLint GetAttribLocation(GLuint programID, std::string name);
	GLint GetUniformLocation(GLuint programID, std::string name);

	//Window stuff
    SDL_Window* m_pWindow;
    const char* WINDOW_TITLE = "MD2 Loader";
    const int WINDOW_WIDTH = 1280;
    const int WINDOW_HEIGHT = 800;

	//GL program variabls
	GLuint m_programId;
	GLuint m_vertexShaderId;
	GLuint m_fragmentShaderId;

	//Shader locations
	GLuint m_modelVertsLocation;
	GLuint m_modelNormsLocation;
	GLuint m_modelViewProjectionMatrixLocation;
	GLuint m_modelViewMatrixLocation;
	GLuint m_normalMatrixLocation;

	//Other
    std::vector<Mesh> m_meshes;
	glm::mat4 m_projection;
};
