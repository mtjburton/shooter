#pragma once

#include "glm/glm.hpp"

#include "RenderingSystem.h"
#include "Transform.h"

struct Particle
{
    Mesh mesh;
    Transform transform;
    float lifetime;
};

