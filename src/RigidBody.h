#pragma once

#include "glm/glm.hpp"

class Transform;

struct RigidBody
{
    Transform* parentTransform = nullptr;

    float mass = 10;
    glm::vec3 velocity = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 force = glm::vec3(0.0f, 0.0f, 0.0f);
};
