#pragma once

#include <GL/glew.h>

struct Mesh
{
public:
    int elementBufferCount;
    GLenum drawType;
    GLuint vertexBufferId;
    GLuint elementBufferId;
    GLuint normalBufferId;
};

struct MeshData
{
public:
    GLushort* pElementData;
    GLfloat* pVertexData;
    GLfloat* pNormalData;

    int elementBufferCount;
    int vertCount;
    GLenum drawType;
};

