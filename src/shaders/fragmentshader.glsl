#version 120

uniform float fade_factor;
uniform sampler2D textures[2];

uniform vec3 lightPos;

varying vec2 fragTexCoord;
varying vec3 fragNormal;
varying vec3 fragVert;

void main()
{
//    gl_FragColor = mix(
//        texture2D(textures[0], texcoord),
//        texture2D(textures[1], texcoord),
//        fade_factor
//    );

    vec3 d = vec3(50.0, 50.0, 20.0) - fragVert;
    float b = clamp(dot(fragNormal, d) / (length(d) * length(fragNormal)), 0.0, 1.0);

    vec3 surfaceColor = vec3(0.9, 0.9, 0.9);
    vec3 lightIntensity = vec3(0.4, 0.4, 0.4);

    gl_FragColor = vec4(b * surfaceColor * lightIntensity, 1.0);
}
