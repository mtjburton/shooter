#version 100

attribute vec3 vert;
attribute vec3 normal;

uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 normalMatrix;

varying vec2 fragTexCoord;
varying vec3 fragNormal;
varying vec3 fragVert;

void main()
{
    fragNormal = (normalMatrix * vec4(normal, 1.0)).xyz;
    fragVert = (normalize(modelViewMatrix * vec4(vert, 1.0))).xyz;

    gl_Position = modelViewProjectionMatrix * vec4(vert, 1.0);

//    texcoord = position * vec2(0.5) + vec2(0.5);
}
