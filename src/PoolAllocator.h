#pragma once

#include <iostream>
#include "Containers.h"

//Warning, this allocator can become fragmented pretty easily
//But if removing and adding in roughly equal amounts then it'll stay reasonably cache friendly

template <typename T>
class PoolAllocator
{
public:
    PoolAllocator(int* pMemory, int objectCount)
        : m_freeSpace(objectCount)
    {
        m_pMemory = pMemory;
        m_pStackTop = pMemory;
        m_memorySize = objectCount * sizeof(T);
        m_objectSize = sizeof(T);
    }

    int* GetEmpty()
    {
        int* allocated = nullptr;

        if(m_freeSpace.Size() < 1)
        {
            allocated = m_pStackTop;
            m_pStackTop += m_objectSize;

#ifdef CHECKS
            if(m_pStackTop - m_pMemory > m_memorySize)
            {
                std::cerr << "\033[0;31mPool allocator has run out of memory!\033[0m"
                    << std::endl;
                assert(0);
            }
#endif
        }
        else
        {
            allocated = m_freeSpace.Top();
            m_freeSpace.Pop();
        }

        return allocated;
    }

    void Release(T* object)
    {
        object->alive = false;
        m_freeSpace.Push(reinterpret_cast<int*>(object));
    }

    T* New()
    {
        return new(GetEmpty()) T();
    }

private:
    int* m_pMemory;
    int* m_pStackTop;
    long m_memorySize;
    long m_objectSize;

    Stack<int*> m_freeSpace;
};
