#ifdef __APPLE__
	#include <SDL2/SDL.h>
#else
	#include <SDL.h>
#endif

#include <iostream>

#include "RenderingSystem.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

RenderingSystem::RenderingSystem()
{

}

RenderingSystem::~RenderingSystem()
{
	SDL_DestroyWindow(m_pWindow);

	for(auto mesh : m_meshes)
	{
		if(glIsBuffer(mesh.vertexBufferId))
		{
			glDeleteBuffers(1, &mesh.vertexBufferId);
		}

		if(glIsBuffer(mesh.elementBufferId))
		{
			glDeleteBuffers(1, &mesh.elementBufferId);
		}
	}

	glDeleteShader(m_vertexShaderId);
	glDeleteShader(m_fragmentShaderId);
	glDeleteProgram(m_programId);
}

void RenderingSystem::Init()
{
    //Set up SDL
    if(SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        std::cout << "Error initialising SDL: " << SDL_GetError() << std::endl;
    }

    atexit(SDL_Quit);

    //Create a window with OpenGL context
    m_pWindow = SDL_CreateWindow(WINDOW_TITLE,
            SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
            WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_OPENGL);

    SDL_GL_CreateContext(m_pWindow);

    if(m_pWindow == nullptr)
    {
        std::cout << "Error creating window: " << SDL_GetError() << std::endl;
    }

    //Set up GLEW
    if(glewInit() != GLEW_OK)
    {
        std::cout << "Error initialising GLEW" << std::endl;
    }

#ifndef EMSCRIPTEN
    if(!glewIsSupported("GL_VERSION_2_0"))
    {
        std::cout << "Cannot initialise glew as OpenGL 2.0 is not supported" << std::endl;
    }
#endif

	//Set up OpenGL proram
	std::string vertexShader = LoadShaderFile("shaders/vertexshader");
	std::string fragmentShader = LoadShaderFile("shaders/fragmentshader");

	m_vertexShaderId = MakeShader(GL_VERTEX_SHADER, vertexShader);
	m_fragmentShaderId = MakeShader(GL_FRAGMENT_SHADER, fragmentShader);

	m_programId = glCreateProgram();
	glAttachShader(m_programId, m_vertexShaderId);
	glAttachShader(m_programId, m_fragmentShaderId);
	glLinkProgram(m_programId);

	GLint success = GL_FALSE;
	glGetProgramiv(m_programId, GL_LINK_STATUS, &success);

	//log out some debug info if program linking failed
	if(success == GL_FALSE)
	{
		GLint logSize = 0;
		glGetProgramiv(m_programId, GL_INFO_LOG_LENGTH, &logSize);

		char* error = new char[logSize];
		glGetShaderInfoLog(m_programId, logSize, NULL, error);

        std::cout << "Failed to create and link GPU program: " << error << std::endl;
		delete[] error;
	}

	//Set some GL flags
	glClearColor(0.0f, 0.5f, 0.5f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
    //glCullFace(GL_CW);

	//Grab shader locations
	m_modelVertsLocation = GetAttribLocation(m_programId, "vert");
    m_modelNormsLocation = GetAttribLocation(m_programId, "normal");

    //Enable attrib arrays
    glEnableVertexAttribArray(m_modelNormsLocation);


	m_modelViewProjectionMatrixLocation =
        GetUniformLocation(m_programId, "modelViewProjectionMatrix");

    m_modelViewMatrixLocation = GetUniformLocation(m_programId, "modelViewMatrix");
    m_normalMatrixLocation = GetUniformLocation(m_programId, "normalMatrix");

	glUseProgram(m_programId);

	//Calculate the screen ratio and create the projection matrix
    //Note if screen ratio is changed the procjection matrix needs to be rebuilt
	float ratio = static_cast<float>(WINDOW_WIDTH) / static_cast<float>(WINDOW_HEIGHT);
    float fov = glm::radians(45.0f);
    m_projection = glm::perspective(fov, ratio, 0.1f, 300.0f);
}

void RenderingSystem::ClearScreen()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void RenderingSystem::Draw(Mesh* pMesh, glm::mat4 mvMatrix)
{
    glm::mat4 mvp = m_projection * mvMatrix;
    glm::mat4 n = glm::transpose(glm::inverse(mvMatrix));

    glUniformMatrix4fv(m_modelViewProjectionMatrixLocation, 1, GL_FALSE, &mvp[0][0]);
    glUniformMatrix4fv(m_modelViewMatrixLocation, 1, GL_FALSE, &mvMatrix[0][0]);
    glUniformMatrix4fv(m_normalMatrixLocation, 1, GL_FALSE, &n[0][0]);

    glBindBuffer(GL_ARRAY_BUFFER, pMesh->vertexBufferId);
    glEnableVertexAttribArray(m_modelVertsLocation);
    glVertexAttribPointer(m_modelVertsLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glBindBuffer(GL_ARRAY_BUFFER, pMesh->normalBufferId);
    glVertexAttribPointer(m_modelNormsLocation, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, pMesh->elementBufferId);
    glDrawElements(pMesh->drawType, pMesh->elementBufferCount, GL_UNSIGNED_SHORT, NULL);
}

void RenderingSystem::DisplayBackBuffer()
{
	SDL_GL_SwapWindow(m_pWindow);
}

GLint RenderingSystem::GetUniformLocation(GLuint programID, std::string name)
{
    GLint uniformLocation = glGetUniformLocation(programID, name.c_str());

    if(uniformLocation == -1)
    {
        std::cout << "Error getting uniform " << name
            << " from vertex shader" << std::endl;
    }

    return uniformLocation;
}

GLint RenderingSystem::GetAttribLocation(GLuint programID, std::string name)
{
    GLint attribLoc = glGetAttribLocation(programID, name.c_str());

    if(attribLoc == -1)
    {
        std::cout << "Error getting attrib " << name
            << " from vertex shader" << std::endl;
    }

    return attribLoc;
}

GLuint RenderingSystem::MakeBuffer(GLenum type, const void* data, GLsizei size)
{
	GLuint bufferId;
	glGenBuffers(1, &bufferId);
	glBindBuffer(type, bufferId);
	glBufferData(type, size, data, GL_STATIC_DRAW);

	return bufferId;
}

GLuint RenderingSystem::MakeShader(GLenum type, std::string src)
{
	GLuint shaderID = glCreateShader(type);
	const GLchar* shaderSrc = src.c_str();

	glShaderSource(shaderID, 1, &shaderSrc, NULL);
	glCompileShader(shaderID);

	GLint isCompiled = GL_FALSE;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &isCompiled);

	if(isCompiled == GL_FALSE)
	{
		GLint logSize = 0;
		glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logSize);

		//The maxLength includes the NULL character
		char* error = new char[logSize];
		glGetShaderInfoLog(shaderID, logSize, NULL, error);

        std::cout << "Error compiling: " << type << std::endl
            << src << std::endl << error << std::endl;

		glDeleteShader(shaderID); //Don't leak the shader.
		shaderID = -1;
		delete[] error;
	}

	return shaderID;
}

std::string RenderingSystem::LoadShaderFile(std::string filename)
{
#ifdef EMSCRIPTEN
	filename += ".web";
#endif

	filename += ".glsl";

	std::stringstream contents;
	std::ifstream file(filename);

	if(file.is_open())
	{
		contents << file.rdbuf();
	}
	else
	{
        std::cout << "Error opening file: " << filename << std::endl;
	}

	return contents.str();
}

Mesh RenderingSystem::BuildMesh(MeshData md)
{
    Mesh m;
    m.elementBufferCount = md.elementBufferCount;
    m.drawType = md.drawType;

    m.vertexBufferId = MakeBuffer(GL_ARRAY_BUFFER, md.pVertexData,
             md.vertCount * 3 * 3 * sizeof(GLfloat));

    m.normalBufferId = MakeBuffer(GL_ARRAY_BUFFER, md.pNormalData,
            md.vertCount * 3 * 3 * sizeof(GLfloat));

    m.elementBufferId = MakeBuffer(GL_ELEMENT_ARRAY_BUFFER, md.pElementData,
            md.elementBufferCount * (md.drawType == GL_QUADS ? 4:3) * sizeof(GLushort));

    m_meshes.push_back(m);

    return m;
}

