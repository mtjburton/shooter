#include <iostream>
#include <sstream>
#include <fstream>

#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/transform.hpp"
#include "glm/gtx/norm.hpp"

#include "Globals.h"
#include "Particle.h"
#include "Entity.h"
#include "RigidBody.h"
#include "StackAllocator.h"
#include "PoolAllocator.h"

#include "Transform.cpp"
#include "RenderingSystem.cpp"
#include "Logger.cpp"
#include "ParticleSystem.cpp"

//Leave this here for now...
Mesh LoadCube()
{
     MeshData md;
     md.elementBufferCount = 6 * 4;
     md.vertCount = 24;
     md.drawType = GL_QUADS;
     md.pVertexData = new GLfloat[md.vertCount * 3]
     {
         //RIGHT
         0.5, 0.5, 0.5,
         0.5, 0.5, -0.5,
         0.5, -0.5, -0.5,
         0.5, -0.5, 0.5,

         //LEFT
         -0.5, 0.5, 0.5,
         -0.5, 0.5, -0.5,
         -0.5, -0.5, -0.5,
         -0.5, -0.5, 0.5,

         //FRONT
         0.5, 0.5, 0.5,
         0.5, -0.5, 0.5,
         -0.5, -0.5, 0.5,
         -0.5, 0.5, 0.5,

         //BACK
         0.5, 0.5, -0.5,
         0.5, -0.5, -0.5,
         -0.5, -0.5, -0.5,
         -0.5, 0.5, -0.5,

         //TOP
         0.5, 0.5, 0.5,
         0.5, 0.5, -0.5,
         -0.5, 0.5, -0.5,
         -0.5, 0.5, 0.5,

         //BOTTOM
         0.5, -0.5, -0.5,
         0.5, -0.5, 0.5,
         -0.5, -0.5, 0.5,
         -0.5, -0.5, -0.5,

     };

     md.pElementData = new GLushort[md.elementBufferCount]
     {
         0,1,2,3,
         4,5,6,7,
         8,9,10,11,
         12,13,14,15,
         16,17,18,19,
         20,21,22,23
     };

     md.pNormalData = new GLfloat[md.vertCount * 3]
     {
         1,0,0,
         1,0,0,
         1,0,0,
         1,0,0,

         -1,0,0,
         -1,0,0,
         -1,0,0,
         -1,0,0,

         0,0,1,
         0,0,1,
         0,0,1,
         0,0,1,

         0,0,-1,
         0,0,-1,
         0,0,-1,
         0,0,-1,

         0,1,0,
         0,1,0,
         0,1,0,
         0,1,0,

         0,-1,0,
         0,-1,0,
         0,-1,0,
         0,-1,0,
     };

     return g->pRenderingSystem->BuildMesh(md);
}

extern "C"
{
    void Load(int* pMemory, int stackSize)
    {
        g = new(pMemory) Globals { nullptr, nullptr, nullptr, nullptr};
        int* pMemoryStart = pMemory + sizeof(Globals);

        g->pBSA = new(pMemoryStart) StackAllocator(pMemoryStart + sizeof(StackAllocator),
                stackSize);

        g->pRenderingSystem = g->pBSA->New<RenderingSystem>();
        g->pRenderingSystem->Init();

        Mesh mesh = LoadCube();

        g->pPlayer = g->pBSA->New<Player>();
        g->pPlayer->mesh = mesh;
        g->pPlayer->rigidBody.parentTransform = &g->pPlayer->transform;
        g->pPlayer->transform.MoveTo(glm::vec3(0.0f, 0.0f, -10.0f));
        g->pPlayer->transform.Scale(0.5f);

        g->pPlayerEngine = g->pBSA->New<ParticleSystem>();
        g->pPlayerEngine->Init(100, mesh);
    }

    void Reload(int* pMemory)
    {
        g = reinterpret_cast<Globals*>(pMemory);
    }

    void Step(float dt)
    {
        const Uint8* pKeyState = SDL_GetKeyboardState(0);

        if(pKeyState[SDL_SCANCODE_UP])
        {
            glm::vec3 f = g->pPlayer->transform.GetForward();
            g->pPlayer->rigidBody.force += f * 100.0f;
        }

        if(pKeyState[SDL_SCANCODE_LEFT])
        {
            g->pPlayer->transform.Rotate(glm::rotate(0.1f, glm::vec3(0.0f, 0.0f, 1.0f)));
        }

        if(pKeyState[SDL_SCANCODE_RIGHT])
        {
            g->pPlayer->transform.Rotate(glm::rotate(-0.1f, glm::vec3(0.0f, 0.0f, 1.0f)));
        }

        g->pRenderingSystem->ClearScreen();

        //TODO: move to physics system
        {
            RigidBody* rb = &g->pPlayer->rigidBody;

            glm::vec3 friction = rb->velocity * 5.0f;
            rb->force -= friction;
            glm::vec3 acc = (rb->force/rb->mass);

            float tv = 20; //Terminal velocity
            float velLength2 = glm::length2(rb->velocity);

            if(velLength2 > tv)
            {
                float diff = velLength2 - tv;
                rb->velocity -= (glm::normalize(rb->velocity) * diff);
            }
            else if(velLength2 < 0.005f)
            {
                rb->velocity = glm::vec3(0.0f, 0.0f, 0.0f);
            }

            rb->parentTransform->MoveBy(rb->velocity * dt);
            rb->velocity += acc * dt;
            rb->force = glm::vec3(0.0f, 0.0f, 0.0f);

            //Wrapping player position, values are hard coded based on camera position
            //although they should be calculated dynamically
            //If the player is moving really slow they might get stuck offscreen
            {
                glm::vec3 pos = rb->parentTransform->GetPosition();

                if(fabs(pos.x) > 7.0)
                {
                    pos.x *= -1;
                }

                if(fabs(pos.y) > 4.5)
                {
                    pos.y *= -1;
                }

                rb->parentTransform->MoveTo(pos);
            }
        }

        g->pRenderingSystem->Draw(&g->pPlayer->mesh, g->pPlayer->transform.Mat4());
        //Will move and render particles
        g->pPlayerEngine->Update(dt);
        g->pRenderingSystem->DisplayBackBuffer();
    }

    void Unload()
    {

    }
}
