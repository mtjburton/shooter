#pragma once

#include <iostream>
#include "Globals.h"
#include "StackAllocator.h"

#include "glm/gtx/transform.hpp"

#include "Mesh.h"
#include "Particle.h"

class ParticleSystem
{
public:
    ParticleSystem();
    void Init(int size, Mesh m);
    void Update(float dt);

private:
    void Spawn(float lifetime, glm::vec3 pos);
    Particle* Get(int index);

    Particle* m_pItems;
    int m_spawnedCount;
    size_t m_objectSize;
    int m_poolSize;
    float m_particleSpawnWait;
    Mesh m_particleMesh;
};
