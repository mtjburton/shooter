#include "Transform.h"
#include "glm/gtx/transform.hpp"

Transform::Transform()
{
    m_position = glm::vec3(0.0f, 0.0f, 0.0f);
    m_scale = glm::vec3(1.0f, 1.0f, 1.0f);
    m_rotation = glm::mat4();
    m_forward = glm::vec4(0.0f, 1.0f, 0.0f, 0.0f);
}

void Transform::MoveTo(glm::vec3 v)
{
    m_position = v;
}

void Transform::MoveBy(glm::vec3 v)
{
    m_position += v;
}

void Transform::Scale(glm::vec3 v)
{
    m_scale *= v;
}

void Transform::Scale(float s)
{
    m_scale *= s;
}

void Transform::Rotate(glm::mat4 m)
{
    m_rotation *= m;
    m_forward =  m * m_forward;
}

glm::vec3 Transform::GetForward()
{
    return glm::vec3(m_forward);
}

glm::mat4 Transform::Mat4()
{
    glm::mat4 scale = glm::scale(glm::mat4(), m_scale);
    glm::mat4 translation = glm::translate(glm::mat4(), m_position);

    return translation * m_rotation * scale;
}

glm::vec3 Transform::GetPosition()
{
    return m_position;
}
