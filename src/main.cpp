#ifdef __APPLE__
	#include <SDL2/SDL.h>
#else
	#include <SDL.h>
#endif

#include <dlfcn.h>

#include <iostream>
#include <memory>

static bool g_gameRunning;

int main()
{
    int stackSize = 1024 * 1024 * 10;
    int* pMemory = new int[stackSize];
    int* pSnapshot = new int[stackSize];
    void* lib = dlopen("libshooter.so", RTLD_NOW);
    void (*funcLoad)(int*, int) = reinterpret_cast<void (*)(int*, int)>(dlsym(lib, "Load"));
    void (*funcReload)(int*) = reinterpret_cast<void (*)(int*)>(dlsym(lib, "Reload"));
    void (*funcStep)(float) = reinterpret_cast<void (*)(float)>(dlsym(lib, "Step"));

    funcLoad(pMemory, stackSize);
    memcpy(pSnapshot, pMemory, stackSize);

    g_gameRunning = true;

    while(g_gameRunning)
    {
        //Eat up all the events in main
        //Game code internally just grabs input state and doesn't try to process events
        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            Uint32 sym = event.key.keysym.sym;
            if(event.type == SDL_QUIT || sym == SDLK_ESCAPE)
            {
                g_gameRunning = false;
            }
        }

        const Uint8* pKeyState = SDL_GetKeyboardState(0);

        if(pKeyState[SDL_SCANCODE_F1])
        {
            memcpy(pSnapshot, pMemory, stackSize);
        }
        else if(pKeyState[SDL_SCANCODE_F2])
        {
            memcpy(pMemory, pSnapshot, stackSize);
        }

        dlclose(lib);
        lib = dlopen("libshooter.so", RTLD_NOW);
        funcReload = reinterpret_cast<void (*)(int*)>(dlsym(lib, "Reload"));
        funcStep = reinterpret_cast<void (*)(float)>(dlsym(lib, "Step"));
        funcReload(pMemory);

        funcStep(33.0f/1000.0f);

        SDL_Delay(33);
    }

    return 0;
}
