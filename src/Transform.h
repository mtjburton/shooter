#pragma once

#include "glm/glm.hpp"

class Transform
{
public:
    Transform();
    void Init(glm::vec3 pos);

    void MoveTo(glm::vec3 v);
    void MoveBy(glm::vec3 v);

    void Scale(float s);
    void Scale(glm::vec3 s);
    void Rotate(glm::mat4 m);

    glm::vec3 GetForward();
    glm::mat4 Mat4();
    glm::vec3 GetPosition();

private:
    glm::vec3 m_scale;
    glm::vec3 m_position;
    glm::vec4 m_forward;
    glm::mat4 m_rotation;
};
