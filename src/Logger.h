#pragma once

#include "glm/glm.hpp"
#include <string>

#ifdef LOGGING
    #define SIMPLE_LOG(x) Log(x)
    #define LOG(x, y) Log(x, y)
#else
    #define SIMPLE_LOG(x)
    #define LOG(x, y)
#endif

void Log(glm::vec2, std::string = "");
void Log(glm::vec3, std::string = "");
void Log(glm::vec4, std::string = "");
void Log(glm::mat4, std::string = "");

void Log(int, std::string = "");
void Log(float, std::string = "");
void Log(double, std::string = "");
void Log(std::string, std::string = "");
